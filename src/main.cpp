#include <iostream>
using std::cout;
using std::cin;
using std::endl;
using std::cerr;

#include <string>
using std::string;

#include <new>
using std::bad_alloc;

#include <limits>

#include "invalida.h"

#include "ord.h"
#include "buscas.h"
using namespace edb1;

#include "util.h"
using namespace util;


int erro_tamanho(int tam)
{
	Tamanho_invalido ex;
	limpa_buffer();
	try{

		if(tam <= 0 || tam >= 0x7fffffff) throw Tamanho_invalido();

		}catch (Tamanho_invalido &inv){
			cerr << ex.what() << endl;
			return 1;
		}catch(...)	{
			cerr << "\nErro desconhecido\n";
			return 1;
		}

	return 0;
}


int main() {
	ClearScreen();

	cout << "\n-------------TESTES-------------\n";
	cout << "\nInforme o tamanho da string a ser testada: ";
	string *a;

	int t_string;
	int erro = 1;
	int erro2 = 1;

	while(erro2 > 0)
	{
		cin >> t_string;
		erro = erro_tamanho(t_string);
		if(erro == 0)
			erro2 = erro_aloc(t_string, a);
	}

	cout << "\nDigite os elementos da string\n";

	for (int i = 0; i < t_string; ++i)
	{
		getline(cin, a[i], '\n');
		cout << endl;
	}

	cout << "\nInforme o tamanho do array de inteiros: ";
	int t_int;
	int *v;

	erro = 1;
	erro2 = 1;

	while(erro2 > 0)
	{
		cin >> t_int;
		erro = erro_tamanho(t_int);
		if(erro == 0)
			erro2 = erro_aloc(t_int, v);
	}


	cout << "\nDigite os elementos do Array\n";

	for (int i = 0; i < t_int; ++i)
	{
		cin >> v[i];
	}
	

	cout << "\nVetor de int: ";
	print_array(v, t_int);
	cout << "String: ";
	print_array(a, t_string);

	cout << "\n----------PÓS-ORDENAÇÃO----------\n";
	merge_sort(v, t_int);
	quick_sort(a, 0, t_string - 1);

	cout << "\n\nVetor de int: ";
	print_array(v, t_string);
	cout << "String: ";
	print_array(a, t_string);


	cout << "\n--------------BUSCAS--------------\n\n";
	cout << "\ninforme a palavra a ser procurada na string: ";
	string m;
	cin.ignore();
	getline(cin, m, '\n');
	cout << "\nB. sequencial i. Posição do elemento: " << b_sequencial_i(m, a, t_string);
	cout << "\nB. sequencial r. Posição do elemento: " << b_sequencial_r(m, a, t_string);
	cout << "\n\nB. binária i. Posição do elemento: " << b_binaria_i(m, a, t_string);
	cout << "\nB. binária r. Posição do elemento: " << b_binaria_r(m, a, 0, t_string);
	cout << "\n\nB. ternária i. Posição do elemento: " << b_ternaria_i(m, a, t_string);
	cout << "\nB. ternária r. Posição do elemento: " << b_ternaria_r(m, a, 0, t_string) << endl;

	cout << "\n\nInforme o número que deseja procurar no vetor de inteiros: ";
	int n;
	cin >> n;

	cout << "\nB. sequencial i. Posição do elemento: " << b_sequencial_i(n, v, t_int);
	cout << "\nB. sequencial r. Posição do elemento: " << b_sequencial_r(n, v, t_int);
	cout << "\n\nB. binária i. Posição do elemento: " << b_binaria_i(n, v, t_int);
	cout << "\nB. binária r. Posição do elemento: " << b_binaria_r(n, v, 0, t_int);
	cout << "\n\nB. ternária i. Posição do elemento: " << b_ternaria_i(n, v, t_int);
	cout << "\nB. ternária r. Posição do elemento: " << b_ternaria_r(n, v, 0, t_int) << endl;

	return 0;
	
	
}