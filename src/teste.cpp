
int ternary_search(int *data, int begin, int end, int key){
	if (begin > end) return false; 
	int middle = (begin + end)/ 2;
	int middle_one = (middle + begin)/2;
	int middle_two = (middle + end)/2;
	
	if(data[middle_one] == key){
			return middle_one;
	}
	if(data[middle_two] == key){
			return middle_two;
	}
	if(data[middle_one] > key){
			return ternary_search(data, begin, middle_one -1, key);
	}
	if(data[middle_two] < key){
			return ternary_search(data, middle_two +1, end, key);
	}else{
		return ternary_search(data, middle_two +1, middle_one -1, key);
	}
}


int b_ternaria_r(int *data, int begin, int end, int key){
	if (begin > end) return -1; 
	int middle = end + (begin - end)/ 2;
	int middle_one = (middle + begin)/2;
	int middle_two = (middle + end)/2;
	
	if(data[middle_one] == key){
			return middle_one;
	}
	if(data[middle_two] == key){
			return middle_two;
	}
	if(data[middle_one] > key){
			return b_ternaria_r(data, begin, middle_one -1, key);
	}
	if(data[middle_two] < key){
			return b_ternaria_r(data, middle_two +1, end, key);
	}else{
		return b_ternaria_r(data, middle_two +1, middle_one -1, key);
	}
}