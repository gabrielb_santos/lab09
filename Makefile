INC_DIR = include
SRC_DIR = src
OBJ_DIR = build
LIB_DIR = lib

CC = g++
CFLAGS = -Wall -pedantic -std=c++11 -ansi -I. -I$(INC_DIR)

linux: util.a util.so gabriel.a gabriel.so prog_estatico prog_dinamico

#LINUX

util.a:$(SRC_DIR)/util.cpp  $(INC_DIR)/util.h 
	$(CC) $(CFLAGS) -c $(SRC_DIR)/util.cpp -o $(OBJ_DIR)/util.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/util.o
	@echo "+++ [Biblioteca estico criada em $(LIB_DIR)/$@] +++"


util.so: $(SRC_DIR)/util.cpp  $(INC_DIR)/util.h
	$(CC) $(CFLAGS) -fPIC -c $(SRC_DIR)/util.cpp -o $(OBJ_DIR)/util.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/util.o
	@echo "+++ [Biblioteca estico criada em $(LIB_DIR)/$@] +++"


gabriel.a: $(SRC_DIR)/main_lib.cpp  $(INC_DIR)/ord.h $(INC_DIR)/buscas.h
	$(CC) $(CFLAGS) -c $(SRC_DIR)/main_lib.cpp -o $(OBJ_DIR)/main_lib.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/main_lib.o
	@echo "+++ [Biblioteca estico criada em $(LIB_DIR)/$@] +++"


gabriel.so: $(SRC_DIR)/main_lib.cpp  $(INC_DIR)/ord.h $(INC_DIR)/buscas.h
	$(CC) $(CFLAGS) -fPIC -c $(SRC_DIR)/main_lib.cpp -o $(OBJ_DIR)/main_lib.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/main_lib.o
	@echo "+++ [Biblioteca estico criada em $(LIB_DIR)/$@] +++"


prog_estatico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/util.a $(LIB_DIR)/gabriel.a -o $(OBJ_DIR)/$@

prog_dinamico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/util.so $(LIB_DIR)/gabriel.so -o $(OBJ_DIR)/$@

clean:
	@rm -rf $(OBJ_)
	@echo "Removendo arquivos objeto e executaveis/binarios..."

go:
	./build/prog_dinamico

go1:
	./build/prog_estatico