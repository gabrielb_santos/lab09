/**
* @file     util.h
* @brief    Arquivo de cabeçalho contendo as definições de funções que realizam
*           operações utilizadas em várias partes do programa.
* @author   Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @author   Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since    01/06/2017
* @date     11/06/2017
*/


#ifndef UTIL_H
#define UTIL_H

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
#include <limits> //numeric_limits

namespace util{
	/** 
	* @brief    Função que limpa o terminal do linux usando ANSI escape codes
	* @details  para mais detalher, acesse o link deixado como comentário ao lado do comando.
	*/
	void ClearScreen();


	/** 
	* @brief    Função que realiza o tratamento de todas as entradas do usuário
	            verificando se são válidas ou não.
	* @details  parte da função retirada do link: http://stackoverflow.com/questions/4798936/numeric-limits-was-not-declared-in-this-scope-no-matching-function-for-call-t
	* @param    num int a ser testado
	*/
	void invalida(float &num);


	/** 
	* @brief    Função que realiza o tratamento de todas as entradas do usuário
	            verificando se são válidas ou não.
	* @details  parte da função retirada do link: http://stackoverflow.com/questions/4798936/numeric-limits-was-not-declared-in-this-scope-no-matching-function-for-call-t
	* @param    num float a ser testado
	*/
	void invalida(int &num);

	template<typename T>
	void print_array(T *v, int tam){
		cout << endl << endl;
		for (int i = 0; i < tam; ++i)
			{
				cout << v[i] << ' ';
			} 

		cout << endl << endl;	
	}

	void limpa_buffer();


}

#endif