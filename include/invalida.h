#ifndef INVALIDA_H
#define INVALIDA_H

#include <exception>
using std::exception;
#include "util.h"

class Tamanho_invalido : public exception
{
	public:
		const char* what()
		{
			return "O tamanho passado é inválido, tente novamente";
		}
	
};

template<typename T>
int erro_aloc(int tam, T *(&a))
{
	Tamanho_invalido ex;
	try{
		a = new T[tam];
	} catch (bad_alloc &ex) {
		cerr << "Erro na alocação de memória: " << ex.what()  << endl;
		return 1;
	}

	return 0;
}


#endif