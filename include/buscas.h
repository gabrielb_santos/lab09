#ifndef BUSCAS_H
#define BUSCAS_H

#include <string>
using std::string;

namespace edb1
{
	template<typename T>
	int b_sequencial_i(T k, T *v, int tam)
	{
		int pos = 0;

		do{
			if (k == v[pos])
			{
				return pos;
			}

			pos++;

		}while(pos < tam);

		return -1;

	}

	template<typename T>
	int b_sequencial_r(T k, T *v, int tam)
	{
		if(tam == -1)
		{
			return -1;

		}else if(v[tam - 1] == k){

			return tam - 1;

		}else{

			return b_sequencial_r(k, v, tam - 1);
		}

	}

	template<typename T>
	int b_binaria_i(T k, T *v, int tamanho)
	{
		int inicio = 0, fim = tamanho-1, meio;

		while(inicio <= fim)
		{
			meio = (inicio + fim)/2;

			if(k < v[meio])
			{
				fim = meio - 1;

			}else if(k > v[meio]){

				inicio = meio + 1;

			}else {

				return meio;
			}
		}

		return - 1;
	
	}


	template<typename T>
	int b_binaria_r(T k, T *v, int inicio, int fim)
	{

		if(inicio > fim){
			return - 1;
		}

		int meio;
		meio = (inicio + fim)/2;

		if(k < v[meio])
		{
			return b_binaria_r(k, v, inicio, meio - 1);

		}else if(k > v[meio]){

			return b_binaria_r(k, v, meio + 1, fim);

		}else{

			return meio;
		} 
	}

	template<typename T>
	int b_ternaria_i(T x, T *v, int tam)
	{
		int lo, hi;
		lo = 0;
		hi = tam-1;

		int mid1, mid2;

		while(lo <= hi)
		{
			mid1 = (lo + hi)/2;
			mid2 = 2 * mid1;

			if(x == v[mid1])
			{
				return mid1;

			}else if( x < v[mid1])
			{
				hi = mid1 - 1;

			}else if(x == v[mid2])
			{
				return mid2;

			}else if(x < v[mid2])
			{
				lo = mid1 + 1;
				hi = mid2 - 1;

			}else
			{
				lo = mid2 + 1;
			}

		}

		return -1;
	}

	template<typename T>
	int b_ternaria_r(T value, T *v, int left, int right)
	{
	    int i;
	    int first,second;
	    if(left>right)
	       return -1;

	    i= (right - left)/3;
	    if(i==0) i++;

	    first = i  + left -1;
	    second = i*2 + left - 1;

	    if(v[first]==value)
	       return first;
	    else
	    if(v[first]>value)
	         return b_ternaria_r(value, v, left, first-1);
	    else
	    {
	        if(v[second]==value)
	          return second;
	        else
	        if(v[second]>value)
	           return b_ternaria_r(value, v, first+1,second-1);
	        else
	           return b_ternaria_r(value, v, second+1,right);
	    }
	}

}

#endif