#ifndef ORD_H
#define ORD_H

namespace edb1
{
	template<typename T>
	int min_i(T *v, int tam, int aux)
	{

		T m = v[aux];

		int pos = aux;

		for (int i = aux; i < tam; ++i)
		{
			if(v[i] < m)
			{
				m = v[i];
				pos = i;
			}
		}

		return pos;
	}



	template<typename T>
	void select_sort(T *v, int tam)
	{
		int min = 0;

		T aux;

		for (int i = 0; i < tam; ++i)
		{
			min = min_i(v, tam, i);

			aux = v[i];
			v[i] = v[min];
			v[min] = aux;
		}
	}

	template<typename T>
	void insert_sort(T *v, int tamanho)
	{
      int i, j;
      T tmp;
      for (i = 1; i < tamanho; i++)
       {
            j = i;

            while (j > 0 && v[j - 1] > v[j])
            {
                  tmp = v[j];
                  v[j] = v[j - 1];
                  v[j - 1] = tmp;
                  j--;
            }
      	}
    }


    template<typename T>
    void bubble_sort(T *v, int tam)
    {
    	int n = tam;

    	int j;
    	bool troca;
    	T aux;
 		j = n - 2;

    	do{

    		troca = false;
    		for(int i = 0; i <= j; ++i)
    		{
	    		if(v[i] > v[i + 1])
	    		{
	    			aux = v[i];
	    			v[i] = v[i + 1];
	    			v[i + 1] = aux;

	    			troca = true;
	    		}
	    	}

    		j--;

    	}while(troca);
    }


	template <typename T> 
	void quick_sort(T *v, int left, int right) {
		int left1 = left, right1 = right;
		T pivot = v[(left + right1) / 2];
		while(left1 <= right1) {
			while(v[left1] < pivot) left1++;
			while(v[right1] > pivot) right1--;
			if(left1 <= right1){
				T tmp = v[left1];
				v[left1] = v[right1];
				v[right1] = tmp;
				left1++;
				right1--;
			}
		}
		if (left < right1) quick_sort(v,left,right1);
		if (left1 < right) quick_sort(v,left1,right);
	}


	template <typename T>
	void intercall(T *v, int n, int k) {
		int p = 0;
		int q = k;
		T *vtmp = new T[n];
		int s =0 ;
		while(p<k && q<n) {
			if (v[p]<=v[q]) vtmp[s++]=v[p++];
			else vtmp[s++]=v[q++];
		}
		while (p<k) vtmp[s++] = v[p++];
		while (q<n) vtmp[s++] = v[q++];
		for (int i = 0; i < n; i++) {
			v[i] = vtmp[i];	
		} 
	}

	template <typename T>
	void merge_sort(T *v, int n) {
		if (n <= 1) return;
		int k = n/2;
		merge_sort(v, k);
		merge_sort(&v[k], n-k);
		intercall(v, n, k);
	}

}

#endif